const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const question1 = (arr, target) => {
  for (let i=0;i<arr.length;i++) {
    for (let j=i+1;j<arr.length;j++) {
      if (arr[i]+arr[j] === target) {
        console.log([i, j]);
        return [i, j]
      }
    }
  }
}

question1([2, 7, 11, 15])
question1([3, 2, 4])

const question2 = (price, paid) => {
  let change = paid - price;
  if ( change < 0)
    return 'จ่ายเงินไม่พอ'

  let money = [1, 2, 5, 10, 20, 50, 100, 500, 1000],
    str = "ทอนด้วย"

  for (let m of money.reverse() ) {
    if ( change >= m ) {
      let amount = Math.floor(change/m)
      let bill = m > 10;
      change %= m;
      str += `${bill ? 'แบงค์':'เหรียญ'} ${m} ${amount} ${bill? 'ใบ': 'เหรียญ'}${change > 0 ?  ',':'' }`;
    }
  }

  return str;
}

app.post("/change", (req, res) => {
  let str_result = question2(req.body.price, req.body.paid)
  return res.send(str_result)
})

app.get("/", (req, res) => {
  return res.send("hello");
});

app.listen(8080);
